#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

// headers for device driver
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>

// macro
#define DEVICE_NAME "char_dev"
#define CHARDRV_MAX_LENGTH 4096
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

struct class *myclass;
struct cdev *mycdev;
struct device *mydevice;
dev_t char_dev;

static char *chardrv_data;
static int chardrv_read_offset, chardrv_write_offset;

static int chardrv_open(struct inode *inode, struct file *file)     // Check device major number
{
    // Device initailization should be implemented here.
    printk("%s\n", __FUNCTION__);
    return 0;
}

static int chardrv_release(struct inode *inode, struct file *file)
{
    printk("%s\n", __FUNCTION__);
    return 0;
}

static ssize_t chardrv_read(struct file *file, char *buf, size_t count, loff_t *ppos)   // Communicate with kernel and checking conditions for device
{
    if ((buf == NULL) || (count < 0))
        return -EINVAL;
    
    if((chardrv_write_offset - chardrv_read_offset) <= 0)
        return 0;

    count = MIN((chardrv_write_offset - chardrv_read_offset), count);
    if (copy_to_user(buf, chardrv_data + chardrv_read_offset, count))
        return -EFAULT;

    chardrv_read_offset += count;

    return count;
}

static ssize_t chardrv_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
    if ((buf == NULL) || (count < 0))
        return -EINVAL;

    if (count + chardrv_write_offset >= CHARDRV_MAX_LENGTH){
        printk("driver space is too small");
        return 0;
    }

    if (copy_from_user(chardrv_data + chardrv_write_offset, buf, count))
        return -EFAULT;

    chardrv_write_offset += count;

    return count;
}

struct file_operations chardrv_fops = {
    .owner = THIS_MODULE,
    .read = chardrv_read,
    .write = chardrv_write,
    .open = chardrv_open,
    .release = chardrv_release
};



static int __init char_drv_init(void)
{
    if (alloc_chrdev_region(&char_dev, 0, 1, DEVICE_NAME) < 0)  // Assign major number of device
        return -EBUSY;

    myclass = class_create(THIS_MODULE, "mycharclass");         // Create 'class' struct for "device_create()"
    if (IS_ERR(myclass)){
        unregister_chrdev_region(char_dev, 1);
        return PTR_ERR(myclass);
    }

    mydevice = device_create(myclass, NULL, char_dev, NULL, "mydevicefile");    // Create & register via "udev"
                                                                                // Device is managed under "/dev" directory
    if (IS_ERR(mydevice)){
        class_destroy(myclass);
        unregister_chrdev_region(char_dev, 1);
        return PTR_ERR(mydevice);
    }

    mycdev = cdev_alloc();                                      // Allocate "cdev" struct instance
    mycdev->ops = &chardrv_fops;
    mycdev->owner = THIS_MODULE;
    if (cdev_add(mycdev, char_dev, 1) < 0){
        device_destroy(myclass, char_dev);
        class_destroy(myclass);
        unregister_chrdev_region(char_dev, 1);
        return -EBUSY;
    }

    chardrv_data = (char *) kmalloc(CHARDRV_MAX_LENGTH * sizeof(char) , GFP_KERNEL);    // Dynamic allocation for driver
    chardrv_read_offset = chardrv_write_offset = 0;

    return 0;
}

static void __exit char_drv_exit(void)
{
    kfree(chardrv_data);
    cdev_del(mycdev);
    device_destroy(myclass, char_dev);
    class_destroy(myclass);
    unregister_chrdev_region(char_dev, 1);
}

module_init(char_drv_init);
module_exit(char_drv_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("YUHO CHOI");
MODULE_DESCRIPTION("Device Driver Test");
MODULE_VERSION("0.1.0");