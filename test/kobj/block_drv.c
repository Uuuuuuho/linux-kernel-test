#include <linux/string.h>
#include <linux/slab.h>
#include <asm/atomic.h>
#include <linux/bio.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/pagemap.h>
#include <linux/blkdev.h>
// #include <linux/genhd.h>
#include <linux/buffer_head.h>
#include <linux/backing-dev.h>
#include <linux/blkpg.h>
#include <linux/writeback.h>

#include <asm/uaccess.h>

#define DEVICE_NAME "blkdrv"
#define BLKDRV_MAX_LENGTH (8 * 1024 * 1024) // 8 MiB
#define BLKDRV_BLK_SIZE 512
#define BLKDRV_TOTAL_BLK (BLKDRV_MAX_LENGTH >> 9)

static int BLKDRV_MAJOR = 0;
static char *blkdrv_data;
struct requiest_queue *blkdrv_queue;
struct gendisk *blkdrv_disk;

static void blkdrv_make_request(struct request_queue *q, struct bio *bio)
{
    struct block_device *bdev = bio->bi_bdev;
    int rw;
    struct bio_vec bvec;
    sector_t sector;
    struct bvec_iter iter;
    int err = -EIO;
    void *mem;
    char *dev;

    sector = bio->bi_iter.bi_sector;
    if (bio_end_sector(bio) > get_capacity(bdev->bd_disk))
        goto out;
    
    if (unlikely(bio->bi_rw & REQ_DISCARD)){
        err = 0;
        goto out;
    }

    rw = bio_rw(bio);
    if (rw == READA)
        rw = READ;

    bio_for_each_segment(bvec, bio, iter) {
        unsigned int len = bvec.bv_len;
        data = blkdrv_data + (sector * BLKDRV_BLK_SIZE);
        mem = kmap_atomic(bvec.bv_page) + bvec.bv_offset;

        if (rw == READ){
            memcpy(mem + bvec.bv_offset, data, len);
            flush_dcache_page(page);
        } else {
            flush_dcache_page(page);
            memcpy(data, mem + bvec.bv_offset, len);
        }

        kunmap_atomic(mem);
        data += len;
        sector += len >> 9;
        err = 0;
    }

out:
    bio_endio(bio, err);
}

int blkdrv_open(struct block_device *dev, fmode_t mode)
{
    return 0;
}

void blkdrv_release(struct gendisk *gd, fmode_t mode)
{
    return ;
}

int blkdrv_ioctl(struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg)
{
    int err;
    if (cmd != BLKFLSBUF)
        return -ENOTTY;

    err = -EBUSY;
    if (bdev->bd_openers <= 1) {
        kill_bdev(bdev);
        err = 0;
    }

    return err;
}

static struct block_device_operation blkdrv_fops = {
    .owner = THIS_MODULE,
    .open = blkdrv_open,
    .release = blkdrv_release,
    .ioctl = blkdrv_ioctl
};

int blkdrv_init(void)
{
    if ((BLKDRV_MAJOR = register_blkdev(BLKDRV_MAJOR, DEVICE_NAME)) < 0) {
        printk("<0> can't be registered \n");
        return -EIO;
    }

    printk("<0> major NO = %d\n", BLKDRV_MAJOR);

    if ((blkdrv_data = vmalloc(BLKDRV_MAX_LENGTH)) == NULL) {
        unregister_glkdev(BLKDRV_MAJOR, DEVICE_NAME);
        printk("<0> vmalloc failed\n");
        return -ENOMEM;
    }

    if ((blkdrv_disk = alloc_disk(1)) == NULL) {
        printk("<0> alloc_disk failed\n");
        unregister_chrdev(BLKDRV_MAJOR, DEVICE_NAME);
        vfree(blkdrv_data);
        return -EIO;
    }

    if ((blkdrv_queue = blk_alloc_queue(GFP_KERNEL)) == NULL) {
        printk("<0> blk_alloc_queue failed \n");
        put_disk(blkdrv_disk);
        vfree(blkdrv_data);
        unregister_chrdev(BLKDRV_MAJOR, DEVICE_NAME);
        return -EIO;
    }

    blk_queue_make_requeset(blkdrv_queue, &blkdrv_make_request);
    blk_queue_max_hw_sectors(blkdrv_queue, 512);
    blkdrv_disk->major = BLKDRV_MAJOR;
    blkdrv_disk->first_minor = 0;
    blkdrv_data->fops = &blkdrv_fops;
    blkdrv_disk->queue = blkdrv_queue;
    sprintf(blkdrv_disk->disk_name, "blkdrv");
    set_capacity(blkdrv_disk, BLKDRV_TOTAL_BLK);
    add_disk(blkdrv_disk);

    return 0;
}

void blkdrv_exit(void)
{
    del_gendisk(blkdrv_disk);
    put_disk(blkdrv_disk);
    blk_cleanup_queue(blkdrv_queue);
    vfree(blkdrv_data);

    blk_unregister_region(MKDEV(BLKDRV_MAJOR, 0), 1);
    unregister_blkdev(BLKDRV_MAJOR, DEVICE_NAME);
}

module_init(blkdrv_init);
module_exit(blkdrv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("YUHO CHOI");
MODULE_DESCRIPTION("Device Driver Test");
MODULE_VERSION("0.1.0");